#!/bin/bash

cek_temu=0
for char_region in *; do

  if [ -d "$char_region" ]; then
    for char_file in "$char_region"/*.jpg; do

      if [ -f "$char_file" ]; then
        char_file_basename=$(basename "$char_file")
        file="${char_file_basename%.*}"
        steghide extract -sf "$char_file" -p "" -xf "$file.txt"

        steg_file="$file.txt"
        cek_code=$(cat "$steg_file")
        cek_res=$(echo -n "$cek_code" | base64 --decode)

        if [[ "$cek_res" == *http* ]]; then

          echo "[$(date '+%d/%m/%y %H:%M:%S')] [FOUND] [$char_file_basename]" >> "image.log"
          cat image.log

          wget "$cek_res"
          cek_temu=1
          break

        else
          echo "[$(date '+%d/%m/%y %H:%M:%S')] [NOT FOUND] [$char_file_basename]" >> "image.log"
          cat image.log

          rm "$steg_file"

        fi
      fi
      sleep 1
    done
  fi

  if [ "$cek_temu" -eq 1 ];then
    break
  fi
done
