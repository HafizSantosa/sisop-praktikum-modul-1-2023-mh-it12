#!/bin/bash

wget -O genshin.zip --no-check-certificate 'https://docs.google.com/uc?export=download&id=1RBuZBtK6q-_I7S_WgeioqIRrUlvRRbq2'
unzip genshin.zip
unzip genshin_character.zip -d genshin_character

region=`awk -F "," '{printf ("%s,", $2)}' list_character.csv `
IFS=',' read -r -a region_arr <<< "$region"
for ((i=1; i<${#region_arr[@]}; i++)); do
  mkdir -p /home/tezla/soal_3/${region_arr[i]}
done

IFS=$'\n' read -d '' -a dataname_arr < <(tail -n +2 list_character.csv)

for path_original in genshin_character/*; do
  name_original=$(echo "${path_original/*\//}")
  decode_name=$(echo "${name_original/.jpg/}" | base64 --decode)

  for char_data in "${dataname_arr[@]}"; do
    char_data_name=$(echo "$char_data" | awk -F, '{print $1}')
    if [ "$decode_name" = "$char_data_name" ]; then
      new_name=$(echo "$char_data" | awk -F, '{print $1 " - " $2 " - " $3 " - " $4}' | tr -d '\r')
      new_file_name="${new_name}.jpg"

      char_loc=$(echo "$char_data" | awk -F, '{print $2}' | tr -d '\r')
      mv genshin_character/"$name_original" "$char_loc"/"$new_file_name"
    fi
  done
done

awk -F ',' 'NR > 1 {print $4}' 'list_character.csv' | sort | uniq -c

rm list_character.csv genshin.zip genshin_character.zip
