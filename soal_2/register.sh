#!/bin/bash
#define functions

llog=/home/tezla/auth.log

regist_name() {

echo -n "Masukkan Username baru anda: "
read input_uname
}

regist_email() {

echo -n "Masukkan Email baru anda: "
read input_email

email_check=$(awk -v tesEmail="${input_email}" '$2~tesEmail {print $2}' /home/tezla/users.txt)
if [ "${input_email}" == "${email_check}" ]
then
        echo "REGISTER FAILED - The email ${email_check} is already used"
        echo "["`date "+%d/%m/%y %H:%M:%S"`"]" "[REGISTER FAILED] ERROR Failed register attempt on user with email ${input_email}" >> "${llog}"
        regist_email
fi
}

regist_pass() {

echo -n "Masukkan Password baru anda: "
read input_pass

if [ "${input_pass}" = "${input_uname}" ]
then
        echo "REGISTER FAILED - password cannot be the same as username"
        echo "["`date "+%d/%m/%y %H:%M:%S"`"]" "[REGISTER FAILED] ERROR Failed register attempt on user with email ${input_email} - password is the same as username" >> "${llog}"
        regist_pass
fi

if [ `echo -n "${input_pass}" | wc -c` -lt "8" ]
then
        echo "REGISTER FAILED - password must at least 8 characters length"
        echo "["`date "+%d/%m/%y %H:%M:%S"`"]" "[REGISTER FAILED] ERROR Failed register attempt on user with email ${input_email} - password is too short" >> "${llog}"
        regist_pass
fi

if [ "${input_pass//[^@#$%&*+=-]/}" = "" ]
then
        echo "REGISTER FAILED - password must contain at least one special character"
        echo "["`date "+%d/%m/%y %H:%M:%S"`"]" "[REGISTER FAILED] ERROR Failed register attempt on user with email ${input_email} - password does not contain any special character" >> "${llog}"
        regist_pass
fi

if [ "${input_pass//[^0-9]/}" = "" ]
then
        echo "REGISTER FAILED - password must contain at least one number"
        echo "["`date "+%d/%m/%y %H:%M:%S"`"]" "[REGISTER FAILED] ERROR Failed register attempt on user with email ${input_email} - password does not contain any number" >> "${llog}"
        regist_pass
fi

if [ "${input_pass}" = "${input_pass^^}" ]
then
        echo "REGISTER FAILED - password must contain at least one lowercase letter"
        echo "["`date "+%d/%m/%y %H:%M:%S"`"]" "[REGISTER FAILED] ERROR Failed register attempt on user with email ${input_email} - password does not contain any lowercase letter" >> "${llog}"
        regist_pass
fi

if [ "${input_pass}" = "${input_pass,,}" ]
then
        echo "REGISTER FAILED - password must contain at least one uppercase letter"
        echo "["`date "+%d/%m/%y %H:%M:%S"`"]" "[REGISTER FAILED] ERROR Failed register attempt on user with email ${input_email} - password does not contain any uppercase letter" >> "${llog}"
        regist_pass
fi
}

regist_name
regist_email
regist_pass

change_pass=$(echo -n "${input_pass}" | base64)
echo "["`date "+%d/%m/%y %H:%M:%S"`"]" "[REGISTER SUCCESS] user ${input_uname} registered successfully" >> "${llog}"
echo "${input_uname} ${input_email} ${change_pass}" >> /home/tezla/users.txt
echo "REGISTER SUCCESS - Welcome, ${input_uname}. Please login to continue"
exit
