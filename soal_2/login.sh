#!/bin/bash
#define functions

llog=/home/tezla/auth.log

loging() {
echo -n "Masukkan email anda: "
read email_check
email=$(awk -v vartest="$email_check" '$2~vartest{print $2}' /home/tezla/users.txt)

if [[ -z "${email}" ]]
then
        echo "LOGIN FAILED - email ${email_check} not registered, please register first"
        echo "["`date "+%d/%m/%y %H:%M:%S"`"]" "[LOGIN FAILED] ERROR a user is tried to login with unexisting email ${email_check}" >> "${llog}"
        exit
fi

uname=$(awk -v vartest="$email_check" '$2~vartest{print $1}' /home/tezla/users.txt)
pass=$(awk -v vartest="$email_check" '$2~vartest{print $3}' /home/tezla/users.txt)

echo -n "Masukkan password anda: "
read pass_input
pass_input=$(echo -n "${pass_input}" | base64)

if [ $pass_input != $pass ]
then
        echo "LOGIN FAILED - invalid password for user with email ${email}"
        echo "["`date "+%d/%m/%y %H:%M:%S"`"]" "[LOGIN FAILED] ERROR Failed login attempt on user with email ${email}" >> "${llog}"
        exit
fi
echo "LOGIN SUCCESS - Welcome, ${uname}"
echo "["`date "+%d/%m/%y %H:%M:%S"`"]" "[LOGIN SUCCESS] user ${uname} logged in successfully" >> "${llog}"
}

loging

exit
