#!/bin/bash

wget --no-check-certificate 'https://docs.google.com/uc?export=download&id=1JYkupCAZTmwPZSZRsB61cklPM-LUjRHp' -O playlist.csv

echo "File Downloaded"

echo "Top 5 Lagu Hip Hop:"

cat playlist.csv | grep "hip hop" | sort -t, -k15 -n -r | head -n 5

echo "Top 5 Unpopular by John Mayer:"

cat playlist.csv | grep "John Mayer" | sort -t, -k15 -n | head -n 5

echo "Top 10 2004 Hits:"

cat playlist.csv | grep "2004" | sort -t, -k15 -n -r | head -n 10

echo "Lagu Edgy:"

cat playlist.csv | grep "Sri"
