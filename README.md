# sisop-praktikum-modul-1-2023-MH-IT12

# Laporan Resmi Praktikum Sistem Operasi Modul 1

Kelompok IT12:
  + Muhammad Afif (5027221032)
  + Hafiz Akmaldi Santosa (5027221061)
  + Nur Azka Rahadiansyah (5027221064)

# Pengantar
Laporan resmi ini dibuat terkait dengan praktikum modul 1 sistem operasi yang telah dilaksanakan pada tanggal xx hingga tanggal xx. Praktikum modul 1 terdiri dari 4 soal dengan tema (bash?) yang dikerjakan oleh kelompok praktikan yang terdiri dari 3 orang selama waktu tertentu.

Kelompok IT12 melakukan pengerjaan modul 1 ini dengan pembagian sebagai berikut:
  + Soal 1 dikerjakan oleh Hafiz Akmaldi S.
  + Soal 2 dikerjakan oleh Muhammad Afif
  + Soal 3 dikerjakan oleh (Hafiz 33%, Afif 33%, Azka 33%); dan
  + Soal 4 dikerjakan bersama oleh Nur Azka R.
Sehingga dengan demikian, semua anggota kelompok memiliki peran yang sama besar dalam pengerjaan modul 1 ini.

Kelompok IT12 juga telah menyelesaikan tugas praktikum modul 1 yang telah diberikan dan telah melakukan demonstrasi kepada Asisten lab Rendy Anfi Yudha. Dari hasil praktikum yang telah dilakukan sebelumnya, maka diperoleh hasil sebagaimana yang dituliskan pada setiap bab di bawah ini.

## Soal 1
(hafiz)
Soal 1 bermaksud untuk membuat sebuah bash file yang dapat menampilkan urutan playlist lagu

### Isi Soal
Farrel ingin membuat sebuah playlist yang sangat edgy di tahun ini. Farrel ingin playlistnya banyak disukai oleh orang-orang lain. Akan tetapi, dia masih bingung sehingga ia memerlukan beberapa data untuk membuat playlist terbaik di dunia. Untung saja Farrel menemukan file playlist.csv yang berisi top lagu pada spotify beserta genrenya.
- Farrel ingin memasukan lagu yang bagus dengan genre hip hop. Oleh karena itu, Farrel perlu melakukan testi terlebih dahulu.       Tampilkan 5 top teratas lagu dengan genre hip hop berdasarkan popularity.
- Karena Farrel merasa kurang dengan lagu tadi, dia ingin mencari 5 lagu yang paling rendah diantara lagu ciptaan John Mayer berdasarkan popularity.
- Karena Farrel takut lagunya kurang enak didengar dia ingin mencari lagu lagi, cari 10 lagu pada tahun 2004 dengan rank popularity tertinggi
- Farrel sangat suka dengan lagu paling keren dan edgy di dunia. Lagu tersebut diciptakan oleh ibu Sri. Bantu Farrel mencari lagu tersebut dengan kata kunci nama pencipta lagu tersebut. 


### Penyelesaian

Dari soal diatas didapatkan penyelesaian dengan bash sebagai berikut:

```bash playlist_keren.sh
#!/bin/bash

wget --no-check-certificate 'https://docs.google.com/uc?export=download&id=1JYkupCAZTmwPZSZRsB61cklPM-LUjRHp' -O playlist.csv

echo "File Downloaded"

echo "Top 5 Lagu Hip Hop:"

cat playlist.csv | grep -i "hip hop" | sort -t, -k15 -n -r | head -n 5

echo "Top 5 Unpopular by John Mayer:"

cat playlist.csv | grep -i "John Mayer" | sort -t, -k15 -n | head -n 5

echo "Top 10 2004 Hits:"

cat playlist.csv | grep -i "2004" | sort -t, -k15 -n -r | head -n 10

echo "Lagu Edgy:"

cat playlist.csv | grep -i "Sri"
```




### Penjelasan
- Langkah pertama yaitu menggunakan command untuk mendownload file yang diberikan dengan command:
```bash
wget --no-check-certificate 'https://docs.google.com/uc?export=download&id=1JYkupCAZTmwPZSZRsB61cklPM-LUjRHp' -O playlist.csv
```
Pada rangkaian command diatas, digunakan
```bash
wget
```
untuk mendownload file,
```bash
--no-check-certificate 'https://docs.google.com/uc?export=download&id=1JYkupCAZTmwPZSZRsB61cklPM-LUjRHp' -O
```
sebagai file yang akan didownload, dan
```bash
playlist.csv
```
sebagai nama file setelah didownload.

- Selanjutnya pada poin pertama soal, kita diharapkan untuk menampilkan top 5 teratas lagu hiphop berdasarkan popularity. Sehingga, didapatkan penyelesaian sebagai berikut:
```bash
cat playlist.csv | grep -i "hip hop" | sort -t, -k15 -n -r | head -n 5
```
pada rangkaian command diatas, digunakan
```bash
cat playlist.csv |
```
untuk melihat ke dalam file,
```bash
| grep -i "hip hop"
```
kemudian pipe untuk mengambil hasil dari command sebelumnya dan diolah dengan command selanjutnya yang mengambil kolom yang memiliki kata kunci hip hop dengan mengabaikan case huruf,
```bash
| sort -t, -k15 -n -r
```
Kembali mengambil hasil dari command sebelumnya untuk di-sort berdasarkan popularity, dimana kolomnya berada pada urutan ke 15 lalu diurutkan berdasarkan nilai yang terbesar ke terkecil,
```bash
| head -n 5
```
dari hasil sorting, didapatkan top 5 lagu hip hop berdasarkan popularity tertinggi ke terendah sebagai berikut:
```
10,Without Me,Eminem,detroit hip hop,2002,112,67,91,-3,24,66,290,0,7,82;;
362,Feel Good Inc.,Gorillaz,alternative hip hop,2005,139,71,82,-7,61,77,223,1,18,80;;
193,The Real Slim Shady,Eminem,detroit hip hop,2000,105,66,95,-4,5,76,284,3,6,80;;
280,Hey Ya!,OutKast,atl hip hop,2003,80,97,73,-2,18,97,235,10,7,79;;
466,Ni**as In Paris,JAY-Z,east coast hip hop,2011,140,86,79,-6,35,77,219,13,32,77;;
```

- Pada poin kedua diharapkan untuk mengurutkan 5 lagu yang memiliki popularity paling rendah milik John Mayer, digunakan penyelesaian sebagai berikut:
```bash
cat playlist.csv | grep -i "John Mayer" | sort -t, -k15 -n | head -n 5
```
Digunakan penyelesaian yang hampir sama untuk poin sebelumnya, perbedaan berada pada sorting dimana kita menggunakan kata kunci yang berbeda yaitu "John Mayer" dan descending sort berdasarkan popularity. Sehingga didapatkan hasil sebagai berikut:
```
157,Heartbreak Warfare,John Mayer,neo mellow,2009,97,55,62,-8,30,31,270,19,2,66;;
694,In the Blood,John Mayer,neo mellow,2017,80,66,55,-7,10,52,244,39,3,66;;
128,Daughters,John Mayer,neo mellow,2003,125,41,67,-8,10,50,238,84,3,68;;
111,Waiting On the World to Change,John Mayer,neo mellow,2006,177,76,58,-6,25,66,201,16,12,74;;
222,Gravity,John Mayer,neo mellow,2006,124,33,76,-10,24,24,246,73,3,75;;
```

- Penyelesaian poin ketiga kurang lebih sama dengan poin pertama, dimana kita diharapkan untuk mencari 10 lagu pada tahun 2004 dengan popularity tertinggi. Didapatkan penyelesaian sebagai berikut:
```bash
cat playlist.csv | grep -i "2004" | sort -t, -k15 -n -r | head -n 10
```
Didapatkan penyelesaian yang kurang lebih sama dengan penyelesaian poin pertama, perbedaanya berada pada nilai
```bash
head -n 10
```
karena untuk mendapatkan 10 data teratas berdasarkan sorting diperlukan nilai yang sama.

Dari penyelesaian diatas, didapatkan hasil sebagai berikut:
```
399,Do They Know It's Christmas? - 1984 Version,Band Aid,electro,2004,115,64,60,-7,10,35,223,16,4,84;;
44,American Idiot,Green Day,modern rock,2004,186,99,38,-2,37,77,176,0,6,78;;
1038,The Chain - 2004 Remaster,Fleetwood Mac,album rock,1977,152,67,55,-9,5,48,270,1,5,78;;
9,Mr. Brightside,The Killers,modern rock,2004,148,92,36,-4,10,23,223,0,8,77;;
213,Wake Me up When September Ends,Green Day,modern rock,2004,105,81,55,-5,10,15,286,2,3,77;;
338,Unwritten,Natasha Bedingfield,dance pop,2004,100,80,71,-6,8,63,259,1,4,76;;
107,Numb / Encore,JAY-Z,east coast hip hop,2004,107,79,69,-4,58,75,206,6,17,75;;
43,Take Me Out,Franz Ferdinand,alternative rock,2004,105,68,28,-9,14,49,237,0,4,74;;
1033,Go Your Own Way - 2004 Remaster,Fleetwood Mac,album rock,1977,135,94,59,-5,7,83,224,2,4,73;;
1,Sunrise,Norah Jones,adult standards,2004,157,30,53,-14,11,68,201,94,3,71;;
```

- Poin terakhir diharapkan untuk mencari lagu oleh bu Sri, dengan menggunakan command sebagai berikut:
```bash
cat playlist.csv | grep -i "Sri"
```
sehingga didapatkan hasil sebagai berikut:
```
168,Hymne ITS,Ir Sri Amiranti MS,slow pop,1984,123,69,69,-5,9,49,212,39,3,73;;
```
- sehingga dengan menjalankan script bash keseluruhan akan didapatkan hasil sebagai berikut:
```
File Downloaded
Top 5 Lagu Hip Hop:
10,Without Me,Eminem,detroit hip hop,2002,112,67,91,-3,24,66,290,0,7,82;;
362,Feel Good Inc.,Gorillaz,alternative hip hop,2005,139,71,82,-7,61,77,223,1,18,80;;
193,The Real Slim Shady,Eminem,detroit hip hop,2000,105,66,95,-4,5,76,284,3,6,80;;
280,Hey Ya!,OutKast,atl hip hop,2003,80,97,73,-2,18,97,235,10,7,79;;
466,Ni**as In Paris,JAY-Z,east coast hip hop,2011,140,86,79,-6,35,77,219,13,32,77;;
Top 5 Unpopular by John Mayer:
157,Heartbreak Warfare,John Mayer,neo mellow,2009,97,55,62,-8,30,31,270,19,2,66;;
694,In the Blood,John Mayer,neo mellow,2017,80,66,55,-7,10,52,244,39,3,66;;
128,Daughters,John Mayer,neo mellow,2003,125,41,67,-8,10,50,238,84,3,68;;
111,Waiting On the World to Change,John Mayer,neo mellow,2006,177,76,58,-6,25,66,201,16,12,74;;
222,Gravity,John Mayer,neo mellow,2006,124,33,76,-10,24,24,246,73,3,75;;
Top 10 2004 Hits:
399,Do They Know It's Christmas? - 1984 Version,Band Aid,electro,2004,115,64,60,-7,10,35,223,16,4,84;;
44,American Idiot,Green Day,modern rock,2004,186,99,38,-2,37,77,176,0,6,78;;
1038,The Chain - 2004 Remaster,Fleetwood Mac,album rock,1977,152,67,55,-9,5,48,270,1,5,78;;
9,Mr. Brightside,The Killers,modern rock,2004,148,92,36,-4,10,23,223,0,8,77;;
213,Wake Me up When September Ends,Green Day,modern rock,2004,105,81,55,-5,10,15,286,2,3,77;;
338,Unwritten,Natasha Bedingfield,dance pop,2004,100,80,71,-6,8,63,259,1,4,76;;
107,Numb / Encore,JAY-Z,east coast hip hop,2004,107,79,69,-4,58,75,206,6,17,75;;
43,Take Me Out,Franz Ferdinand,alternative rock,2004,105,68,28,-9,14,49,237,0,4,74;;
1033,Go Your Own Way - 2004 Remaster,Fleetwood Mac,album rock,1977,135,94,59,-5,7,83,224,2,4,73;;
1,Sunrise,Norah Jones,adult standards,2004,157,30,53,-14,11,68,201,94,3,71;;
Lagu Edgy:
168,Hymne ITS,Ir Sri Amiranti MS,slow pop,1984,123,69,69,-5,9,49,212,39,3,73;;
```

## Soal 2
Soal 2 bermaksud untuk membuat dua buah bash file. Yang pertama berupa sistem untuk register dan yang kedua berupa sistem untuk login.

### Isi Soal
```
Shinichi merupakan seorang detektif SMA yang kembali menjadi anak kecil karena ulah organisasi hitam. Dengan tubuhnya yang mengecil, Shinichi tidak dapat menggunakan identitas lamanya sehingga harus membuat identitas baru. Selain itu, ia juga harus membuat akun baru dengan identitas nya saat ini. Bantu Shinichi membuat program register dan login agar Shinichi dapat dengan mudah membuat semua akun baru yang ia butuhkan.

Ketentuan register :
  - Password dienkripsi menggunakan base64
  - Password harus lebih dari 8 karakter
  - Password terdiri dari minimal 1 huruf kecil dan 1 huruf besar
  - Password tidak boleh sama dengan username
  - Password terdiri dari minimal 1 angka dan 1 simbol unik
```

### Penyelesaian
register.sh
```
#!/bin/bash
#define functions

llog=/home/tezla/auth.log

regist_name() {

echo -n "Masukkan Username baru anda: "
read input_uname
}

regist_email() {

echo -n "Masukkan Email baru anda: "
read input_email

email_check=$(awk -v tesEmail="${input_email}" '$2~tesEmail {print $2}' /home/tezla/users.txt)
if [ "${input_email}" == "${email_check}" ]
then
        echo "REGISTER FAILED - The email ${email_check} is already used"
        echo "["`date "+%d/%m/%y %H:%M:%S"`"]" "[REGISTER FAILED] ERROR Failed register attempt on user with email ${input_email}" >> "${llog}"
        regist_email
fi
}

regist_pass() {

echo -n "Masukkan Password baru anda: "
read input_pass

if [ "${input_pass}" = "${input_uname}" ]
then
        echo "REGISTER FAILED - password cannot be the same as username"
        echo "["`date "+%d/%m/%y %H:%M:%S"`"]" "[REGISTER FAILED] ERROR Failed register attempt on user with email ${input_email} - password is the same as username" >> "${llog}"
        regist_pass
fi

if [ `echo -n "${input_pass}" | wc -c` -lt "8" ]
then
        echo "REGISTER FAILED - password must at least 8 characters length"
        echo "["`date "+%d/%m/%y %H:%M:%S"`"]" "[REGISTER FAILED] ERROR Failed register attempt on user with email ${input_email} - password is too short" >> "${llog}"
        regist_pass
fi

if [ "${input_pass//[^@#$%&*+=-]/}" = "" ]
then
        echo "REGISTER FAILED - password must contain at least one special character"
        echo "["`date "+%d/%m/%y %H:%M:%S"`"]" "[REGISTER FAILED] ERROR Failed register attempt on user with email ${input_email} - password does not contain any special character" >> "${llog}"
        regist_pass
fi

if [ "${input_pass//[^0-9]/}" = "" ]
then
        echo "REGISTER FAILED - password must contain at least one number"
        echo "["`date "+%d/%m/%y %H:%M:%S"`"]" "[REGISTER FAILED] ERROR Failed register attempt on user with email ${input_email} - password does not contain any number" >> "${llog}"
        regist_pass
fi

if [ "${input_pass}" = "${input_pass^^}" ]
then
        echo "REGISTER FAILED - password must contain at least one lowercase letter"
        echo "["`date "+%d/%m/%y %H:%M:%S"`"]" "[REGISTER FAILED] ERROR Failed register attempt on user with email ${input_email} - password does not contain any lowercase letter" >> "${llog}"
        regist_pass
fi

if [ "${input_pass}" = "${input_pass,,}" ]
then
        echo "REGISTER FAILED - password must contain at least one uppercase letter"
        echo "["`date "+%d/%m/%y %H:%M:%S"`"]" "[REGISTER FAILED] ERROR Failed register attempt on user with email ${input_email} - password does not contain any uppercase letter" >> "${llog}"
        regist_pass
fi
}

regist_name
regist_email
regist_pass

change_pass=$(echo -n "${input_pass}" | base64)
echo "["`date "+%d/%m/%y %H:%M:%S"`"]" "[REGISTER SUCCESS] user ${input_uname} registered successfully" >> "${llog}"
echo "${input_uname} ${input_email} ${change_pass}" >> /home/tezla/users.txt
echo "REGISTER SUCCESS - Welcome, ${input_uname}. Please login to continue"
exit
```

login.sh
```
#!/bin/bash
#define functions

llog=/home/tezla/auth.log

loging() {
echo -n "Masukkan email anda: "
read email_check
email=$(awk -v vartest="$email_check" '$2~vartest{print $2}' /home/tezla/users.txt)

if [[ -z "${email}" ]]
then
        echo "LOGIN FAILED - email ${email_check} not registered, please register first"
        echo "["`date "+%d/%m/%y %H:%M:%S"`"]" "[LOGIN FAILED] ERROR a user is tried to login with unexisting email ${email_check}" >> "${llog}"
        exit
fi

uname=$(awk -v vartest="$email_check" '$2~vartest{print $1}' /home/tezla/users.txt)
pass=$(awk -v vartest="$email_check" '$2~vartest{print $3}' /home/tezla/users.txt)

echo -n "Masukkan password anda: "
read pass_input
pass_input=$(echo -n "${pass_input}" | base64)

if [ $pass_input != $pass ]
then
        echo "LOGIN FAILED - invalid password for user with email ${email}"
        echo "["`date "+%d/%m/%y %H:%M:%S"`"]" "[LOGIN FAILED] ERROR Failed login attempt on user with email ${email}" >> "${llog}"
        exit
fi
echo "LOGIN SUCCESS - Welcome, ${uname}"
echo "["`date "+%d/%m/%y %H:%M:%S"`"]" "[LOGIN SUCCESS] user ${uname} logged in successfully" >> "${llog}"
}

loging

exit
```

### Penjelasan
register.sh dibagi menjadi beberapa bagian fungsi yang kemudian dipanggil pada bagian akhir script. Dengan demikian, bash file tidak perlu dijalankan ulang ketika user ingin mengulang saat memasukkan input.

Namun sebelum membuat fungsi, dibuat variabel llog untuk menyingkat path file auth.log yang digunakan untuk menyimpan log dari input yang dimasukkan user
```
llog=/home/tezla/auth.log
```

Kemudian dibuat berbagai fungsi yang menjadi bagian-bagian utama dalam file register.sh

- Fungsi regist_name
```
regist_name() {

echo -n "Masukkan Username baru anda: "
read input_uname
}
```

Fungsi ini merupakan fungsi sederhana yang terdiri dari command echo untuk menampilkan pesan sebelum user memasukkan input serta command read untuk memasukkan input user ke dalam variabel `input_name`

- Fungsi regist_email
```
regist_email() {

echo -n "Masukkan Email baru anda: "
read input_email

email_check=$(awk -v tesEmail="${input_email}" '$2~tesEmail {print $2}' /home/tezla/users.txt)
if [ "${input_email}" == "${email_check}" ]
then
        echo "REGISTER FAILED - The email ${email_check} is already used"
        echo "["`date "+%d/%m/%y %H:%M:%S"`"]" "[REGISTER FAILED] ERROR Failed register attempt on user with email ${input_email}" >> "${llog}"
        regist_email
fi
}
```
Pada fungsi ini, input dari user akan dimasukkan ke dalam variabel `input_email`.

```
email_check=$(awk -v tesEmail="${input_email}" '$2~tesEmail {print $2}' /home/tezla/users.txt)
```
Kemudian digunakan command awk untuk mencari apakah isi dari variabel input_email terdapat di dalam file `users.txt` dan hasil pencarian tersebut akan dimasukkan ke dalam variabel `email_check`.

```
if [ "${input_email}" == "${email_check}" ]
then
```
Jika nilai dari input_email sama dnegan email_check maka hal tersbut menunjukkan bahwa email yang diinput oleh user telah digunakan sebelumnya.

```
echo "REGISTER FAILED - The email ${email_check} is already used"
        echo "["`date "+%d/%m/%y %H:%M:%S"`"]" "[REGISTER FAILED] ERROR Failed register attempt on user with email ${input_email}" >> "${llog}"
        regist_email
```

Ketika email telah digunakan, maka nilai register akan gagal. Command echo pertama dijalankan untuk memberi alert pada user bahwa register yang dilakukan gagal karena email yang dimasukkan sudah pernah didaftarkan. Sementara command echo yang kedua akan dimasukkan ke dalam line baru pada `auth.log` untuk mencatat log terkait dengan gagal register yang dilakukan oleh seorang user.

Fungsi `regist_email` kemudian dipanggil ulang sehingga user dapat kembali memasukkan email yang ingin didaftarkan.

- Fungsi regist_pass
```
regist_pass() {

echo -n "Masukkan Password baru anda: "
read input_pass

if [ "${input_pass}" = "${input_uname}" ]
then
        echo "REGISTER FAILED - password cannot be the same as username"
        echo "["`date "+%d/%m/%y %H:%M:%S"`"]" "[REGISTER FAILED] ERROR Failed register attempt on user with email ${input_email} - password is the same as username" >> "${llog}"
        regist_pass
fi

if [ `echo -n "${input_pass}" | wc -c` -lt "8" ]
then
        echo "REGISTER FAILED - password must at least 8 characters length"
        echo "["`date "+%d/%m/%y %H:%M:%S"`"]" "[REGISTER FAILED] ERROR Failed register attempt on user with email ${input_email} - password is too short" >> "${llog}"
        regist_pass
fi

if [ "${input_pass//[^@#$%&*+=-]/}" = "" ]
then
        echo "REGISTER FAILED - password must contain at least one special character"
        echo "["`date "+%d/%m/%y %H:%M:%S"`"]" "[REGISTER FAILED] ERROR Failed register attempt on user with email ${input_email} - password does not contain any special character" >> "${llog}"
        regist_pass
fi

if [ "${input_pass//[^0-9]/}" = "" ]
then
        echo "REGISTER FAILED - password must contain at least one number"
        echo "["`date "+%d/%m/%y %H:%M:%S"`"]" "[REGISTER FAILED] ERROR Failed register attempt on user with email ${input_email} - password does not contain any number" >> "${llog}"
        regist_pass
fi

if [ "${input_pass}" = "${input_pass^^}" ]
then
        echo "REGISTER FAILED - password must contain at least one lowercase letter"
        echo "["`date "+%d/%m/%y %H:%M:%S"`"]" "[REGISTER FAILED] ERROR Failed register attempt on user with email ${input_email} - password does not contain any lowercase letter" >> "${llog}"
        regist_pass
fi

if [ "${input_pass}" = "${input_pass,,}" ]
then
        echo "REGISTER FAILED - password must contain at least one uppercase letter"
        echo "["`date "+%d/%m/%y %H:%M:%S"`"]" "[REGISTER FAILED] ERROR Failed register attempt on user with email ${input_email} - password does not contain any uppercase letter" >> "${llog}"
        regist_pass
fi
}
```
Fungsi `regist_pass` ini terdiri dari 6 kondisi yang digunakan untuk memeriksa apakah password yang dimasukkan telah sesuai dengan ketentuan atau tidak.

```
echo -n "Masukkan Password baru anda: "
read input_pass
```
Fungsi dimulai dengan command read untuk memasukkan input user ke dalam variabel `input_pass`. Kemudian akan berlanjut kepada bagian pengondisian yang terdiri dari kondisi-kondisi sebagai Berikut

  + jika password sama dengan username
  ```
  if [ "${input_pass}" = "${input_uname}" ]
then
        echo "REGISTER FAILED - password cannot be the same as username"
        echo "["`date "+%d/%m/%y %H:%M:%S"`"]" "[REGISTER FAILED] ERROR Failed register attempt on user with email ${input_email} - password is the same as username" >> "${llog}"
        regist_pass
fi
  ```
  Fungsi if digunakan untuk memeriksa pakaha isi dari variabel `input_pass` bernilai sama dengan `input_uname`. Jika ya, maka sistem akan menampilkan pesan melalui command echo pertama pada fungsi, lalu mangirimkan log `REGISTER FAILED` ke dalam baris baru pada auth.log untuk pencatatan. Terakhir, fungsi `regist_pass` akan dipanggil kembali sehingga user dapat kembali memasukkan password.

  Namun jika password tidak bernilai sama dengan username, maka sistem akan melanjutkan ke pengondisian berikutnya.

  + jika password kurang dari 8 karakter
  ```
  if [ `echo -n "${input_pass}" | wc -c` -lt "8" ]
then
        echo "REGISTER FAILED - password must at least 8 characters length"
        echo "["`date "+%d/%m/%y %H:%M:%S"`"]" "[REGISTER FAILED] ERROR Failed register attempt on user with email ${input_email} - password is too short" >> "${llog}"
        regist_pass
fi
  ```

  Fungsi `wc` digunakan untuk menghitung jumlah karakter pada isi yang ditampilkan variabel `input_pass` melalui command echo. kemudian tanda `-lt "8"` digunakan untuk memeriksa apakah hasil perhitungan sebelumnya bernilai kurang dari 8 atau tidak.

  Jika ya, maka sistem akan menampilkan pesan `REGISTER FAILED` dan seterusnya sebagaimana pada pengondisian sebelumnya.

  Jika tidak, sistem akan melanjutkan ke pengondisian selanjutnya.

  + jika password tidak memiliki karakter spesial
  ```
  if [ "${input_pass//[^@#$%&*+=-]/}" = "" ]
then
        echo "REGISTER FAILED - password must contain at least one special character"
        echo "["`date "+%d/%m/%y %H:%M:%S"`"]" "[REGISTER FAILED] ERROR Failed register attempt on user with email ${input_email} - password does not contain any special character" >> "${llog}"
        regist_pass
fi
  ```
pengondisian ini akan mendeteksi apakah password yang dimasukkan tidak mengandung setidaknya 1 karakter dari kumpulan karakter berikut
```
^@#$%&*+=-
```
Jika ya, maka sistem akan menampilkan pesan `REGISTER FAILED` dan seterusnya sebagaimana pada pengondisian sebelumnya.

  Jika tidak, sistem akan melanjutkan ke pengondisian selanjutnya.

  + jika password tidak mengandung angka
  ```
  if [ "${input_pass//[^0-9]/}" = "" ]
then
        echo "REGISTER FAILED - password must contain at least one number"
        echo "["`date "+%d/%m/%y %H:%M:%S"`"]" "[REGISTER FAILED] ERROR Failed register attempt on user with email ${input_email} - password does not contain any number" >> "${llog}"
        regist_pass
fi
  ```
  Mirip dengan pengondisian sebelumnya, pengondisian ini akan mendeteksi apakah password yang dimasukkan tidak mengandung setidaknya 1 angka dari 0 hingga 9.

  Jika ya, maka sistem akan menampilkan pesan `REGISTER FAILED` dan seterusnya sebagaimana pada pengondisian sebelumnya.

  Jika tidak, sistem akan melanjutkan ke pengondisian selanjutnya.

  + jika password tidak mengandung huruf kecil
  ```
if [ "${input_pass}" = "${input_pass^^}" ]
then
        echo "REGISTER FAILED - password must contain at least one lowercase letter"
        echo "["`date "+%d/%m/%y %H:%M:%S"`"]" "[REGISTER FAILED] ERROR Failed register attempt on user with email ${input_email} - password does not contain any lowercase letter" >> "${llog}"
        regist_pass
fi
  ```
  Pengondisian ini akan melihat apakah, ketika semua huruf pada variabel `iput_pass` dijadikan huruf kapital, nilai akan sama ketika belum dikapitalkan. Jika bernilai benar, maka hal itu berarti password yang diinputkan tidak memiliki huruf kecil.

  Jika ya, maka sistem akan menampilkan pesan `REGISTER FAILED` dan seterusnya sebagaimana pada pengondisian sebelumnya.

  Jika tidak, sistem akan melanjutkan ke pengondisian selanjutnya.

  + jika password tidak mengandung huruf kapital
  ```
  if [ "${input_pass}" = "${input_pass,,}" ]
then
        echo "REGISTER FAILED - password must contain at least one uppercase letter"
        echo "["`date "+%d/%m/%y %H:%M:%S"`"]" "[REGISTER FAILED] ERROR Failed register attempt on user with email ${input_email} - password does not contain any uppercase letter" >> "${llog}"
        regist_pass
fi
  ```
  Kebalikan dari pengondisian sebelumnya, pengondisian ini apakah ketika semua huruf pada isi variabel `input_pass` dikecilkan, hasilnya akan sama dengan ketika belum dikecilkan. Jika bernilai benar, maka hal itu berarti tidak ada huruf kapital yang digunakan.

  Jika ya, maka sistem akan menampilkan pesan `REGISTER FAILED` dan seterusnya sebagaimana pada pengondisian sebelumnya.

  Jika tidak, maka sistem akan menyelesaikan fungsi `regist_pass` dan melanjutkan ke fungsi berikutnya.

+ Memanggil semua fungsi yang telah dituliskan
```
regist_name
regist_email
regist_pass
```
Ketika variabel `input_uname`, `input_email` telah berhasil dimasukkan dan kemudian `input_pass` telah berhasil melalui fungsi `regist_pass`, maka semua kondisi register telaha terpenuhi.

```
change_pass=$(echo -n "${input_pass}" | base64)
echo "${input_uname} ${input_email} ${change_pass}" >> /home/tezla/users.txt
```
Variabel `input_pass` kemudian akan dienkripsi menggunakan fungsi base64 dan hasilnya akan dimasukkan ke dalam variabel `change_pass`.

Akhirnya, variabel `input_uname`, `input_email`, dan `change_pass` akan diformat dalam bentuk 1 baris yang dipisahkan menggunakan spasi dan diletakkan pada baris baru di file `users.txt`.

```
echo "["`date "+%d/%m/%y %H:%M:%S"`"]" "[REGISTER SUCCESS] user ${input_uname} registered successfully" >> "${llog}"
echo "REGISTER SUCCESS - Welcome, ${input_uname}. Please login to continue"
```
Log sukses register kemudian akan dikirimkan pada line baru di `auth.log` dan user juga akan mendapatkan pesan `REGISTER SUKSES` sebagai penunjuk bahwa register yang dilakukan telah berhasil.

Pada file kedua, `login.sh`, hanya terdapat satu buah fungsi utama, yaitu `loging` yang berisi semua command untuk melakukan login.
```
echo -n "Masukkan email anda: "
read email_check
email=$(awk -v vartest="$email_check" '$2~vartest{print $2}' /home/tezla/users.txt)
```
Fungsi dimulai dengan user memasukkan input email yang kemudian disimpan pada variabel `email_check`. Variabel `email` juga dibuat dengan menggunakan command awk untuk mengambil nilai pada `users.txt` yang bernilai sama dengan variabel `email_check`.

```
if [[ -z "${email}" ]]
then
        echo "LOGIN FAILED - email ${email_check} not registered, please register first"
        echo "["`date "+%d/%m/%y %H:%M:%S"`"]" "[LOGIN FAILED] ERROR a user is tried to login with unexisting email ${email_check}" >> "${llog}"
        exit
fi
```
Jika variabel `email` tidak memiliki nilai, maka hal tersebut menunjukkan bahwa email yang diinputkan tidak terdaftar pada `users.txt`. Dengan demikian, dicatatlah log `LOGIN FAILED` pada file `auth.log` dan kemudian fungsi loging akan dihentikan.

```
uname=$(awk -v vartest="$email_check" '$2~vartest{print $1}' /home/tezla/users.txt)
pass=$(awk -v vartest="$email_check" '$2~vartest{print $3}' /home/tezla/users.txt)
```
Jika variabel `email` memiliki nilai, maka akan dibuat dua buah variabel baru bernama `uname` dan `pass` yang kemudian diisi menggunakan keluaran dari command awk untuk mengambil bagian pertama dan ketiga dari baris user pada file `users.txt`.

```
echo -n "Masukkan password anda: "
read pass_input
pass_input=$(echo -n "${pass_input}" | base64)
```
Fungsi kemudian berlanjut pada bagian memasukkan password. Hasil yang diinputkan akan disimpan pada variabel `pass_input` dan kemudian dienkripsikan menggunakan base64.

```
if [ $pass_input != $pass ]
then
        echo "LOGIN FAILED - invalid password for user with email ${email}"
        echo "["`date "+%d/%m/%y %H:%M:%S"`"]" "[LOGIN FAILED] ERROR Failed login attempt on user with email ${email}" >> "${llog}"
        exit
fi
```
Pengondisian dilakukan untuk memeriksa apakah password yang diinputkan (`pass_input`) bernilai sama dengan password hyang telah terdaftar pada email yang dimasukkan (`pass`).

Jika password yang diinputkan tidak bernilai sama, maka sistem akan menampilkan pesan `LOGIN FAILED` pada user dan juga menyimpan log pada file `auth.log`.

```
echo "LOGIN SUCCESS - Welcome, ${uname}"
echo "["`date "+%d/%m/%y %H:%M:%S"`"]" "[LOGIN SUCCESS] user ${uname} logged in successfully" >> "${llog}"
}
```
Namun jika password yang diinputkan bernilai sama dnegan yang telah tersimpan, maka sistem akan menampilkan pesan `LOGIN SUCCESS` dan menyimpan log ke dalam file `auth.log`. 

Bash file kemudian akan berhenti.

## Soal 3
(Bersama)
Soal ini mengharuskan kita untuk melakukan decode, sorting, serta mencari flag

### Isi Soal
Soal ini mengharapkan kita untuk membuat script yang akan digunakan untuk decode nama dari file .jpg yang didownload melalui tautan yang diberikan. Kemudian hasil decode tersebut akan di-rename kembali berdasarkan data yang ada di file list_character.csv, setiap file dikumpulkan kedalam folder berdasarkan region tiap character. Lalu kita diharapkan untuk menghitung serta menampilkan jumlah pengguna untuk setiap senjata yang ada, kemudian menghapus file yang tidak digunakan.
Script kedua digunakan untuk mencari flag yaitu dengan melakukan pengecekan terhadap setiap file tiap satu detik. Pengecekan dilakukan dengan cara meng-ekstrak tiap gambar dengan menggunakan command steghide. Dalam setiap gambar tersebut, terdapat sebuah file txt yang berisi string. Kemudian mulai melakukan dekripsi dengan base64 pada tiap file txt dan mendapatkan sebuah url. Setelah mendapatkan url yang dicari, langsung menghentikan program find_me.sh serta mendownload file berdasarkan url yang didapatkan. setiap kali  melakukan ekstraksi dan ternyata hasil ekstraksi bukan yang diinginkan, maka akan langsung menghapus file txt tersebut. Namun, jika itu merupakan file txt yang dicari, maka akan menyimpan hasil dekripsi-nya bukan hasil ekstraksi. Selain itu juga, Aether melakukan pencatatan log pada file image.log untuk setiap pengecekan gambar.

### Penyelesaian

- genshin.sh
```bash
#!/bin/bash

wget -O genshin.zip --no-check-certificate 'https://docs.google.com/uc?export=download&id=1RBuZBtK6q-_I7S_WgeioqIRrUlvRRbq2'
unzip genshin.zip
unzip genshin_character.zip -d genshin_character

region=`awk -F "," '{printf ("%s,", $2)}' list_character.csv `
IFS=',' read -r -a region_arr <<< "$region"
for ((i=1; i<${#region_arr[@]}; i++)); do
  mkdir -p /home/tezla/soal_3/${region_arr[i]}
done

IFS=$'\n' read -d '' -a dataname_arr < <(tail -n +2 list_character.csv)

for path_original in genshin_character/*; do
  name_original=$(echo "${path_original/*\//}")
  decode_name=$(echo "${name_original/.jpg/}" | base64 --decode)

  for char_data in "${dataname_arr[@]}"; do
    char_data_name=$(echo "$char_data" | awk -F, '{print $1}')
    if [ "$decode_name" = "$char_data_name" ]; then
      new_name=$(echo "$char_data" | awk -F, '{print $1 " - " $2 " - " $3 " - " $4}' | tr -d '\r')
      new_file_name="${new_name}.jpg"

      char_loc=$(echo "$char_data" | awk -F, '{print $2}' | tr -d '\r')
      mv genshin_character/"$name_original" "$char_loc"/"$new_file_name"
    fi
  done
done

awk -F ',' 'NR > 1 {print $4}' 'list_character.csv' | sort | uniq -c

rm list_character.csv genshin.zip genshin_character.zip

```

-find_me.sh
```bash
#!/bin/bash

cek_temu=0
for char_region in *; do

  if [ -d "$char_region" ]; then
    for char_file in "$char_region"/*.jpg; do

      if [ -f "$char_file" ]; then
        char_file_basename=$(basename "$char_file")
        file="${char_file_basename%.*}"
        steghide extract -sf "$char_file" -p "" -xf "$file.txt"

        steg_file="$file.txt"
        cek_code=$(cat "$steg_file")
        cek_res=$(echo -n "$cek_code" | base64 --decode)

        if [[ "$cek_res" == *http* ]]; then

          echo "[$(date '+%d/%m/%y %H:%M:%S')] [FOUND] [$char_file_basename]" >> "image.log"
          cat image.log

          wget "$cek_res"
          cek_temu=1
          break

        else
          echo "[$(date '+%d/%m/%y %H:%M:%S')] [NOT FOUND] [$char_file_basename]" >> "image.log"
          cat image.log

          rm "$steg_file"

        fi
      fi
      sleep 1
    done
  fi

  if [ "$cek_temu" -eq 1 ];then
    break
  fi
done

```
### Penjelasan
- Script genshin.sh digunakan untuk menyelesaikan fase pertama dari soal yaitu untuk decode dan sorting. Berikut langkah penyelesaiannya:

1. Download dan unzip file yang diberikan kemudian unzip file yang ada di dalamnya
```bash
wget -O genshin.zip --no-check-certificate 'https://docs.google.com/uc?export=download&id=1RBuZBtK6q-_I7S_WgeioqIRrUlvRRbq2'
unzip genshin.zip
unzip genshin_character.zip -d genshin_character
```
2. Menggunakan awk untuk membaca kolom kedua (indeks 2) dari setiap baris dalam file "list_character.csv" dan menggabungkannya menjadi satu string, yang kemudian disimpan dalam variabel "region". Memecah string "region" menjadi array dengan delimiter (pemisah) koma dan menyimpan hasilnya dalam array "region_arr".
```bash
region=`awk -F "," '{printf ("%s,", $2)}' list_character.csv `
IFS=',' read -r -a region_arr <<< "$region"
for ((i=1; i<${#region_arr[@]}; i++)); do
  mkdir -p /home/tezla/soal_3/${region_arr[i]}
done

```
Awal dari loop for yang akan digunakan untuk membuat direktori berdasarkan nilai-nilai dalam array "region_arr". Membuat direktori dengan nama yang ada dalam array "region_arr" di dalam direktori "/home/tezla/soal_3/". Opsi "-p" digunakan untuk memastikan bahwa direktori yang ada dalam path juga dibuat jika belum ada.

3. Menggunakan "tail" untuk mengambil seluruh baris dari file "list_character.csv" kecuali baris pertama, dan kemudian memecahnya menjadi array "dataname_arr" dengan delimiter newline.
```bash
IFS=$'\n' read -d '' -a dataname_arr < <(tail -n +2 list_character.csv)
```

4. loop for yang akan digunakan untuk melakukan operasi pada file-file dalam direktori "genshin_character". Mengambil nama file dari path file yang sedang diiterasi lalu menghapus ekstensi ".jpg" dari nama file, dan kemudian mendekode nama tersebut dari base64.
```bash
for path_original in genshin_character/*; do
  name_original=$(echo "${path_original/*\//}")
  decode_name=$(echo "${name_original/.jpg/}" | base64 --decode)
```
5. Nested loop yang akan digunakan untuk mencocokkan nama karakter dengan data karakter dalam array "dataname_arr". menggunakan awk untuk mengambil nama karakter dari data karakter yang sedang diiterasi. membandingkan nama karakter yang telah didekode dengan nama karakter dalam data. menggabungkan beberapa kolom data karakter menjadi satu string baru, dan menghapus karakter carriage return ("\r") jika ada.
```bash
 for char_data in "${dataname_arr[@]}"; do
    char_data_name=$(echo "$char_data" | awk -F, '{print $1}')
    if [ "$decode_name" = "$char_data_name" ]; then
      new_name=$(echo "$char_data" | awk -F, '{print $1 " - " $2 " - " $3 " - " $4}' | tr -d '\r')
      new_file_name="${new_name}.jpg"

      char_loc=$(echo "$char_data" | awk -F, '{print $2}' | tr -d '\r')
      mv genshin_character/"$name_original" "$char_loc"/"$new_file_name"
    fi
  done
done

```
Membuat nama file baru dengan format yang ditentukan. Mendapatkan lokasi karakter dari data karakter yang sedang diiterasi. Memindahkan file karakter dari direktori asalnya ke direktori tujuan dengan nama baru.

6. Ini adalah perintah yang menggunakan awk untuk mengambil nilai kolom keempat (indeks 4) dari baris-baris dalam file "list_character.csv", mengurutkannya, dan menghitung jumlah uniknya kemudian menghapus file-file yang telah digunakan dalam script ini setelah selesai dijalankan.
```bash
awk -F ',' 'NR > 1 {print $4}' 'list_character.csv' | sort | uniq -c

rm list_character.csv genshin.zip genshin_character.zip
```

Setelah script tersebut dijalankan, akan mendapat hasil seperti ini:
```
  8 Bow
 12 Catalyst
  6 Claymore
  6 Polearm
 10 Sword
```
Dan isi direktori seperti ini:
```
.
├── Fontaine
│   ├── Lynette - Fontaine - Anemo - Sword.jpg
│   └── Lyney - Fontaine - Pyro - Bow.jpg
├── Inazuma
│   ├── Arataki Itto - Inazuma - Geo - Claymore.jpg
│   ├── Ayaka - Inazuma - Cyro - Sword.jpg
│   ├── Ayato - Inazuma - Hydro - Sword.jpg
│   ├── Kaedehara Kazuha - Inazuma - Anemo - Sword.jpg
│   ├── Kujou Sara - Inazuma - Electro - Bow.jpg
│   ├── Raiden Shogun - Inazuma - Electro - Polearm.jpg
│   ├── Sangonomiya Kokomi - Inazuma - Hydro - Catalyst.jpg
│   ├── Sayu - Inazuma - Anemo - Claymore.jpg
│   ├── Yae Miko - Inazuma - Electro - Catalyst.jpg
│   └── Yoimiya - Inazuma - Pyro - Claymore.jpg
├── Liyue
│   ├── Ganyu - Liyue - Cyro - Bow.jpg
│   ├── Hu Tao - Liyue - Pyro - Polearm.jpg
│   ├── Keqing - Liyue - Electro - Sword.jpg
│   ├── Ningguang - Liyue - Geo - Catalyst.jpg
│   ├── Qiqi - Liyue - Cyro - Catalyst.jpg
│   ├── Xiangling - Liyue - Pyro - Polearm.jpg
│   ├── Xiao - Liyue - Anemo - Polearm.jpg
│   ├── Xingqiu - Liyue - Hydro - Sword.jpg
│   ├── Yanfei - Liyue - Pyro - Catalyst.jpg
│   └── Zhongli - Liyue - Geo - Polearm.jpg
├── Mondstat
│   ├── Albedo - Mondstat - Geo - Sword.jpg
│   ├── Barbara - Mondstat - Hydro - Catalyst.jpg
│   ├── Bennett - Mondstat - Pyro - Sword.jpg
│   ├── Chongyun - Mondstat - Cyro - Claymore.jpg
│   ├── Diluc - Mondstat - Pyro - Claymore.jpg
│   ├── Diona - Mondstat - Cyro - Bow.jpg
│   ├── Fischl - Mondstat - Electro - Catalyst.jpg
│   ├── Klee - Mondstat - Pyro - Catalyst.jpg
│   ├── Lisa - Mondstat - Electro - Catalyst.jpg
│   └── Venti - Mondstat - Anemo - Bow.jpg
├── Sumeru
│   ├── Alhaitham - Sumeru - Dendro - Sword.jpg
│   ├── Baizhu - Sumeru - Dendro - Catalyst.jpg
│   ├── Collei - Sumeru - Dendro - Bow.jpg
│   ├── Cyno - Sumeru - Electro - Polearm.jpg
│   ├── Faruzan - Sumeru - Anemo - Bow.jpg
│   ├── Kaveh - Sumeru - Dendro - Claymore.jpg
│   ├── Nahida - Sumeru - Dendro - Catalyst.jpg
│   ├── Nilou - Sumeru - Hydro - Sword.jpg
│   ├── Tighnari - Sumeru - Dendro - Bow.jpg
│   └── Wanderer - Sumeru - Anemo - Catalyst.jpg
├── genshin.sh
└── genshin_character
```
Kemudian berlanjut ke fase kedua soal yaitu mencari flag.

7. Script find_me.sh berfungsi untuk mencari tautan HTTP yang mungkin tersembunyi dalam file gambar dalam berbagai direktori. Alur kerja kode dimulai dengan menginisialisasi variabel `cek_temu` ke nilai `0`. Selanjutnya, kode memulai dua level loop. Yang pertama `(for char_region in *; do)` berfungsi untuk melakukan iterasi melalui semua objek dalam direktori saat ini, sementara yang kedua `(for char_file in "$char_region"/*.jpg; do)` akan melakukan iterasi melalui semua file gambar dengan ekstensi ".jpg" dalam setiap direktori yang ditemukan. 
```bash
#!/bin/bash

cek_temu=0
for char_region in *; do

  if [ -d "$char_region" ]; then
    for char_file in "$char_region"/*.jpg; do

      if [ -f "$char_file" ]; then
        char_file_basename=$(basename "$char_file")
        file="${char_file_basename%.*}"
        steghide extract -sf "$char_file" -p "" -xf "$file.txt"

        steg_file="$file.txt"
        cek_code=$(cat "$steg_file")
        cek_res=$(echo -n "$cek_code" | base64 --decode)

        if [[ "$cek_res" == *http* ]]; then

          echo "[$(date '+%d/%m/%y %H:%M:%S')] [FOUND] [$char_file_basename]" >> "image.log"
          cat image.log

          wget "$cek_res"
          cek_temu=1
          break

        else
          echo "[$(date '+%d/%m/%y %H:%M:%S')] [NOT FOUND] [$char_file_basename]" >> "image.log"
          cat image.log

          rm "$steg_file"

        fi
      fi
      sleep 1
    done
  fi

  if [ "$cek_temu" -eq 1 ];then
    break
  fi
done
```
Pada setiap iterasi, kode memeriksa apakah objek yang sedang diiterasi adalah sebuah direktori atau sebuah file yang ada. Jika itu adalah file yang ada, maka kode menggunakan steghide untuk mencoba mengekstrak data tersembunyi dari file gambar tersebut. Hasil ekstraksi disimpan dalam file teks dengan nama yang sama seperti file gambar tanpa ekstensi. Kemudian, isi dari file teks tersebut dibaca dan didekode dari base64.

Selanjutnya, kode memeriksa apakah isi dari file teks yang telah didekode mengandung tautan HTTP. Jika tautan HTTP ditemukan, informasi tentang tautan tersebut dicatat dalam file `image.log`, beserta waktu dan nama file gambar yang berkaitan. Kemudian, kode mengunduh tautan HTTP tersebut. Jika tidak ada tautan HTTP yang ditemukan dalam file tersebut, maka file teks hasil ekstraksi akan dihapus.

Kode juga menggunakan variabel `cek_temu` untuk melacak apakah tautan HTTP telah ditemukan. Jika tautan HTTP pertama kali ditemukan, maka variabel `cek_temu` diubah menjadi `1`, dan kode akan keluar dari loop pertama. Dengan demikian, pencarian akan berhenti setelah menemukan tautan HTTP pertama dalam salah satu file gambar dalam direktori.

Setelah menjalankan script tersebut akan didapatkan direktori sebagai berikut:
![Alt text](http://drive.google.com/uc?export=view&id=127sMHUPjsE74mHA7J1x7VToYh21HtiVJ)


## Soal 4
(Azka)
Soal nomer bermaksud untuk membuat dua buah script bash yaitu script program monitoring resource (per menit) dan agregasi data dari log hasil monitoring (per jam)

### Isi Soal
Defatra sangat tergila-gila dengan laptop legion nya. Suatu hari, laptop legion nya mendadak rusak 🙁 Tentu saja, Defatra adalah programer yang harus 24/7 ngoding tanpa menggunakan vscode di laptop legion nya.  Akhirnya, dia membawa laptop legion nya ke tukang servis untuk diperbaiki. Setelah selesai diperbaiki, ternyata biaya perbaikan sangat mahal sehingga dia harus menggunakan uang hasil slot nya untuk membayarnya. Menurut Kang Servis, masalahnya adalah pada laptop Defatra yang overload sehingga mengakibatkan crash pada laptop legion nya. Untuk mencegah masalah serupa di masa depan, Defatra meminta kamu untuk membuat program monitoring resource yang tersedia pada komputer.
Buatlah program monitoring resource pada laptop kalian. Cukup monitoring ram dan monitoring size suatu directory. Untuk ram gunakan command `free -m`. Untuk disk gunakan command `du -sh <target_path>`. Catat semua metrics yang didapatkan dari hasil `free -m`. Untuk hasil `du -sh <target_path>` catat size dari path directory tersebut. Untuk target_path yang akan dimonitor adalah /home/{user}/. 
A. Masukkan semua metrics ke dalam suatu file log bernama metrics_{YmdHms}.log. {YmdHms} adalah waktu disaat file script bash kalian dijalankan. Misal dijalankan pada 2023-01-31 15:00:00, maka file log yang akan tergenerate adalah metrics_20230131150000.log. 
B. Script untuk mencatat metrics diatas diharapkan dapat berjalan otomatis pada setiap menit. 
C. Kemudian, buat satu script untuk membuat agregasi file log ke satuan jam. Script agregasi akan memiliki info dari file-file yang tergenerate tiap menit. Dalam hasil file agregasi tersebut, terdapat nilai minimum, maximum, dan rata-rata dari tiap-tiap metrics. File agregasi akan ditrigger untuk dijalankan setiap jam secara otomatis. Berikut contoh nama file hasil agregasi metrics_agg_2023013115.log dengan format metrics_agg_{YmdH}.log 
D. Karena file log bersifat sensitif pastikan semua file log hanya dapat dibaca oleh user pemilik file. 


### Penyelesaian
#1. minute_log.sh

#!/bin/bash
# minute_log.sh

#while true; do
  # Mendapatkan tanggal dan waktu saat ini dalam format YmdHms
  timestamp=$(date "+%Y%m%d%H%M%S")

  # Menjalankan perintah untuk memonitor RAM
  ram_info=$(free -m | awk 'NR==2 {print $2","$3","$4","$5","$6","$7","}')
  ram_info=${ram_info/ /,}  # Mengganti spasi dengan koma

  swap_info=$(free -m | awk 'NR==3 {print $2","$3","$4","}')

  # Menentukan target directory yang akan dimonitor (ganti {user} dengan nama pengguna)
  target_path="/home/tezla/"

  # Menjalankan perintah untuk memonitor size dari target directory
  disk_info=$(du -sh "$target_path" | awk '{print ","$1}')

  # Menyimpan semua metrics ke dalam file log dengan format metrics_YmdHms.log
  echo "mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size" >> "/home/tezla/log/metrics_$timestamp.log"
  echo "$ram_info$swap_info$target_path$disk_info" >> "/home/tezla/log/metrics_$timestamp.log"

  # Tunggu sampai detik awal berikutnya sebelum iterasi berikutnya
#  sleep $((60 - $(date "+%S")))
#done

#2. aggregate_minutes_to_hourly_log.s

#!/bin/bash
# aggregate_minutes_to_hourly_log.sh

# Mendapatkan tanggal dan waktu saat ini dalam format YmdH
timestamp_hourly=$(date "+%Y%m%d%H")

# Menentukan direktori tempat semua file log terletak
log_directory="/home/tezla/log"

# Mencari semua file log yang sesuai dengan format metrics_YmdHms.log
log_files=$(find "$log_directory" -name "metrics_*.log" -not -name "*agg*")

# Inisialisasi nilai minimum, maksimum, dan total untuk setiap metrik
declare -A metrics_min
declare -A metrics_max
declare -A metrics_total
declare -A metrics_avg

# Inisialisasi nilai minimum dan maksimum untuk path_size
min_path_size=999999999
max_path_size=0
total_path_size=0
log_count=0

# Loop melalui semua file log dan melakukan agregasi
for log_file in $log_files; do
  # Mendapatkan tanggal dan waktu dari nama file log
  log_timestamp=$(basename "$log_file" | cut -c 9-22)

  # Menambahkan 1 ke hitungan log
  log_count=$((log_count + 1))

  # Menjalankan perintah untuk memonitor RAM dan mendapatkan path dan path_size
  ram_info=$(cat "${log_file}" | awk 'NR == 2 {print $0}')
  ram_info=$(echo "${ram_info/\,\/home*/}")
  path_info=$(cat "${log_file}" | awk 'NR == 2 {print $0}')
  path_info=$(echo "${path_info/*\/\,/}" | head -c +1)

  # Memisahkan data RAM menjadi array
  IFS=',' read -r -a ram_values <<< "$ram_info"

  IFS=',' read -r -a metric_name <<< "mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free"
  # Menghitung nilai minimum dan maksimum untuk setiap metrik
  for ((i=0; i<${#ram_values[@]}; i++)); do
    if [[ -z "${metrics_min[${metric_name[i]}]}" || "${ram_values[i]}" -lt "${metrics_min[${metric_name[i]}]}" ]]; then
      metrics_min[${metric_name[i]}]="${ram_values[i]}"
    fi
    if [[ -z "${metrics_max[${metric_name[i]}]}" || "${ram_values[i]}" -gt "${metrics_max[${metric_name[i]}]}" ]]; then
      metrics_max[${metric_name[i]}]="${ram_values[i]}"
    fi
    metrics_total[${metric_name[i]}]=$((metrics_total[${metric_name[i]}] + ${ram_values[i]}))
  done

  # Menghitung nilai minimum dan maksimum untuk path_size
  if [[ "${path_info[0]}" -lt "$min_path_size" ]]; then
    min_path_size="${path_info[0]}"
  fi
  if [[ "${path_info[0]}" -gt "$max_path_size" ]]; then
    max_path_size="${path_info[0]}"
  fi
  total_path_size="$((total_path_size + path_info[0]))"
done

# Menghitung rata-rata untuk setiap metrik
for ((i=0; i<${#metric_name[@]}; i++)); do
  metrics_avg[${metric_name[i]}]=$((metrics_total[${metric_name[i]}] / $log_count))
done

# Menghitung rata-rata untuk path_size
avg_path_size="$((total_path_size / log_count))"

# Menyimpan hasil agregasi ke dalam file log dengan format metrics_agg_YmdH.log
output_file="$log_directory/metrics_agg_$timestamp_hourly.log"
echo "type,mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size" > "$output_file"
echo "minimum,${metrics_min[mem_total]},${metrics_min[mem_used]},${metrics_min[mem_free]},${metrics_min[mem_shared]},${metrics_min[mem_buff]},${metrics_min[mem_available]},${metrics_min[swap_total]},${metrics_min[swap_used]},0,/home/tezla/,$min_path_size" >> "$output_file"
echo "maximum,${metrics_max[mem_total]},${metrics_max[mem_used]},${metrics_max[mem_free]},${metrics_max[mem_shared]},${metrics_max[mem_buff]},${metrics_max[mem_available]},${metrics_max[swap_total]},${metrics_max[swap_used]},0,/home/tezla/,$max_path_size" >> "$output_file"
echo "average,${metrics_avg[mem_total]},${metrics_avg[mem_used]},${metrics_avg[mem_free]},${metrics_avg[mem_shared]},${metrics_avg[mem_buff]},${metrics_avg[mem_available]},${metrics_avg[swap_total]},${metrics_avg[swap_used]},0,/home/tezla/,$avg_path_size" >> "$output_file"

echo "Agregasi berhasil disimpan dalam file $output_file"

### Penjelasan
#1 minute_log
#timestamp=$(date "+%Y%m%d%H%M%S")
digunakan untuk mendapatkan tanggal dan waktu saat ini dalam format YmdHms dan menyimpannya dalam variabel timestamp.
#ram_info=$(free -m | awk 'NR==2 {print $2","$3","$4","$5","$6","$7","}')
untuk memonitor penggunaan RAM. free -m untuk menampilkan informasi RAM dalam megabyte. awk untuk  output yang dibutuhkan (total RAM, RAM yang digunakan, RAM yang tersedia, RAM yang digunakan bersama, RAM yang digunakan untuk buffer, dan RAM yang tersedia) dipisahkan dengan koma dan disimpan dalam variabel ram_info
#ram_info=${ram_info/ /,}
untuk mengganti spasi dalam variabel ram_info dengan koma.
#swap_info=$(free -m | awk 'NR==3 {print $2","$3","$4","}')
 untuk memonitor penggunaan swap
#target_path="/home/uruser/
menentukan target directory yang akan dimonitor
#disk_info=$(du -sh "$target_path" | awk '{print ","$1}')
 untuk memonitor ukuran dari target directory ($target_path) dengan menggunakan perintah du -sh. Hasilnya diambil, dan ukuran direktori dipisahkan dengan koma dan disimpan dalam variabel disk_info

#echo "mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size" >> "/home/uruser/log/metrics_$timestamp.log"
untuk menambahkan header ke file log yang akan dibuat

#echo "$ram_info$swap_info$target_path$disk_info" >> "/home/uruser/log/metrics_$timestamp.log"
menyimpan semua informasi yang telah dikumpulkan (RAM, swap, path, dan ukuran direktori) ke dalam file log dengan format metrics_YmdHms.log

#2 aggregate_minutes_to_hourly_log
#timestamp_hourly=$(date "+%Y%m%d%H")
untuk mendapatkan tanggal dan waktu saat ini dalam format YmdH (tahun, bulan, tanggal, dan jam) dan menyimpannya dalam variabel timestamp_hourly.
#log_directory="/home/uruser/log"
menentukan direktori tempat semua file log yang akan diagregasi berada. 
#log_files=$(find "$log_directory" -name "metrics_*.log" -not -name "*agg*")
untuk mencari semua file log yang sesuai dengan format metrics_YmdHms.log dalam direktori yang ditentukan ($log_directory).
#declare -A metrics_min, declare -A metrics_max, declare -A metrics_total, declare -A metrics_avg
 untuk mendeklarasikan empat buah associative arrays (array yang menggunakan string sebagai indeks). Masing-masing dari arrays ini akan digunakan untuk menyimpan nilai minimum, maksimum, total, dan rata-rata
#min_path_size=999999999, max_path_size=0, total_path_size=0
untuk menginisialisasi nilai minimum, maksimum, dan total untuk ukuran direktori (path_size).
#log_count=0
untuk menghitung berapa banyak file log yang telah diagregasi.
#for log_file in $log_files; do
awal dari loop yang akan berjalan melalui semua file log yang ditemukan
#log_timestamp=$(basename "$log_file" | cut -c 9-22)
untuk mendapatkan tanggal dan waktu dari nama file log. Bagian cut -c 9-22 digunakan untuk mengambil bagian tanggal dan waktu dari nama file.
#log_count=$((log_count + 1))
untuk menambahkan 1 ke hitungan log.
#ram_info=$(cat "${log_file}" | awk 'NR == 2 {print $0}')
untuk mengambil data RAM dari file log dengan menggunakan perintah cat dan awk. Data RAM ini akan digunakan untuk perhitungan agregasi. Selain itu, awk juga digunakan untuk mengambil data path (yang juga diperlukan untuk perhitungan).
#min_path_size, max_path_size, dan total_path_size
