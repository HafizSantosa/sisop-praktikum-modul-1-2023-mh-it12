#!/bin/bash
# minute_log.sh

#while true; do
  # Mendapatkan tanggal dan waktu saat ini dalam format YmdHms
  timestamp=$(date "+%Y%m%d%H%M%S")

  # Menjalankan perintah untuk memonitor RAM
  ram_info=$(free -m | awk 'NR==2 {print $2","$3","$4","$5","$6","$7","}')
  ram_info=${ram_info/ /,}  # Mengganti spasi dengan koma

  swap_info=$(free -m | awk 'NR==3 {print $2","$3","$4","}')

  # Menentukan target directory yang akan dimonitor (ganti {user} dengan nama pengguna)
  target_path="/home/tezla/"

  # Menjalankan perintah untuk memonitor size dari target directory
  disk_info=$(du -sh "$target_path" | awk '{print ","$1}')

  # Menyimpan semua metrics ke dalam file log dengan format metrics_YmdHms.log
  echo "mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size" >> "/home/tezla/log/metrics_$timestamp.log"
  echo "$ram_info$swap_info$target_path$disk_info" >> "/home/tezla/log/metrics_$timestamp.log"

  # Tunggu sampai detik awal berikutnya sebelum iterasi berikutnya
#  sleep $((60 - $(date "+%S")))
#done
