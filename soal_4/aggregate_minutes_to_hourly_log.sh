#!/bin/bash
# aggregate_minutes_to_hourly_log.sh

# Mendapatkan tanggal dan waktu saat ini dalam format YmdH
timestamp_hourly=$(date "+%Y%m%d%H")

# Menentukan direktori tempat semua file log terletak
log_directory="/home/tezla/log"

# Mencari semua file log yang sesuai dengan format metrics_YmdHms.log
log_files=$(find "$log_directory" -name "metrics_*.log" -not -name "*agg*")

# Inisialisasi nilai minimum, maksimum, dan total untuk setiap metrik
declare -A metrics_min
declare -A metrics_max
declare -A metrics_total
declare -A metrics_avg

# Inisialisasi nilai minimum dan maksimum untuk path_size
min_path_size=999999999
max_path_size=0
total_path_size=0
log_count=0

# Loop melalui semua file log dan melakukan agregasi
for log_file in $log_files; do
  # Mendapatkan tanggal dan waktu dari nama file log
  log_timestamp=$(basename "$log_file" | cut -c 9-22)

  # Menambahkan 1 ke hitungan log
  log_count=$((log_count + 1))

  # Menjalankan perintah untuk memonitor RAM dan mendapatkan path dan path_size
  ram_info=$(cat "${log_file}" | awk 'NR == 2 {print $0}')
  ram_info=$(echo "${ram_info/\,\/home*/}")
  path_info=$(cat "${log_file}" | awk 'NR == 2 {print $0}')
  path_info=$(echo "${path_info/*\/\,/}" | head -c +1)

  # Memisahkan data RAM menjadi array
  IFS=',' read -r -a ram_values <<< "$ram_info"

  IFS=',' read -r -a metric_name <<< "mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free"
  # Menghitung nilai minimum dan maksimum untuk setiap metrik
  for ((i=0; i<${#ram_values[@]}; i++)); do
    if [[ -z "${metrics_min[${metric_name[i]}]}" || "${ram_values[i]}" -lt "${metrics_min[${metric_name[i]}]}" ]]; then
      metrics_min[${metric_name[i]}]="${ram_values[i]}"
    fi
    if [[ -z "${metrics_max[${metric_name[i]}]}" || "${ram_values[i]}" -gt "${metrics_max[${metric_name[i]}]}" ]]; then
      metrics_max[${metric_name[i]}]="${ram_values[i]}"
    fi
    metrics_total[${metric_name[i]}]=$((metrics_total[${metric_name[i]}] + ${ram_values[i]}))
  done

  # Menghitung nilai minimum dan maksimum untuk path_size
  if [[ "${path_info[0]}" -lt "$min_path_size" ]]; then
    min_path_size="${path_info[0]}"
  fi
  if [[ "${path_info[0]}" -gt "$max_path_size" ]]; then
    max_path_size="${path_info[0]}"
  fi
  total_path_size="$((total_path_size + path_info[0]))"
done

# Menghitung rata-rata untuk setiap metrik
for ((i=0; i<${#metric_name[@]}; i++)); do
  metrics_avg[${metric_name[i]}]=$((metrics_total[${metric_name[i]}] / $log_count))
done

# Menghitung rata-rata untuk path_size
avg_path_size="$((total_path_size / log_count))"

# Menyimpan hasil agregasi ke dalam file log dengan format metrics_agg_YmdH.log
output_file="$log_directory/metrics_agg_$timestamp_hourly.log"
echo "type,mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size" > "$output_file"
echo "minimum,${metrics_min[mem_total]},${metrics_min[mem_used]},${metrics_min[mem_free]},${metrics_min[mem_shared]},${metrics_min[mem_buff]},${metrics_min[mem_available]},${metrics_min[swap_total]},${metrics_min[swap_used]},0,/home/tezla/,$min_path_size" >> "$output_file"
echo "maximum,${metrics_max[mem_total]},${metrics_max[mem_used]},${metrics_max[mem_free]},${metrics_max[mem_shared]},${metrics_max[mem_buff]},${metrics_max[mem_available]},${metrics_max[swap_total]},${metrics_max[swap_used]},0,/home/tezla/,$max_path_size" >> "$output_file"
echo "average,${metrics_avg[mem_total]},${metrics_avg[mem_used]},${metrics_avg[mem_free]},${metrics_avg[mem_shared]},${metrics_avg[mem_buff]},${metrics_avg[mem_available]},${metrics_avg[swap_total]},${metrics_avg[swap_used]},0,/home/tezla/,$avg_path_size" >> "$output_file"

echo "Agregasi berhasil disimpan dalam file $output_file"
